package ldh.maker.controller;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import ldh.maker.MainLauncher;
import ldh.maker.ServerMain;
import ldh.maker.model.ModuleType;

import java.net.URL;
import java.util.ResourceBundle;

public class MainLancherController implements Initializable {

    @FXML private ComboBox<ModuleType> moduleChoiceBox;
    @FXML private Label descLabel;

    public void selectAction(ActionEvent actionEvent) {
        ModuleType moduleType = moduleChoiceBox.getSelectionModel().getSelectedItem();
        if (moduleType == null) {
            return;
        }
        Platform.runLater(()->{
            try {
                Application application = (Application) moduleType.getClazz().newInstance();
                if (application instanceof ServerMain) {
                    ServerMain.startDb(moduleType.getDbName());
                }
                Stage stage = MainLauncher.STAGE;
                application.start(stage);
                stage.centerOnScreen();
            } catch (Exception e) {
                e.printStackTrace();
            }

        });
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        moduleChoiceBox.setCellFactory(new Callback<ListView<ModuleType>, ListCell<ModuleType>>() {
            @Override
            public ListCell<ModuleType> call(ListView<ModuleType> p) {
                return new ListCell<ModuleType>() {
                    @Override
                    protected void updateItem(ModuleType item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(item.getName());
                        if (item == null || empty) {
                            setGraphic(null);
                        } else {
                            String value = item.getName();
                            HBox hBox = new HBox();
                            int index = value.lastIndexOf(" *");
                            String s1 = value.substring(0, index);
                            String s2 = value.substring(index);
                            Label label1 = new Label(s1);
                            Label label2 = new Label(s2);
                            label2.setTextFill(Color.RED);
                            hBox.getChildren().addAll(label1, label2);
                            setGraphic(hBox);
                        }
                    }
                };
            }
        });

        moduleChoiceBox.getItems().addAll(ModuleType.values());
        moduleChoiceBox.setConverter(new StringConverter<ModuleType>() {
            @Override
            public String toString(ModuleType object) {
                return object.getName();
            }

            @Override
            public ModuleType fromString(String name) {
                return ModuleType.valueOf(name);
            }
        });

        moduleChoiceBox.getSelectionModel().selectedItemProperty().addListener((b, o, n)->{
            changeModule(n);
        });
    }

    private void changeModule(ModuleType module) {
        descLabel.setText(module.getDesc());
    }
}
