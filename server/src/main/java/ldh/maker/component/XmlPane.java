package ldh.maker.component;

import javafx.application.Platform;
import javafx.scene.control.TextArea;
import javafx.scene.control.TreeItem;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import ldh.maker.code.CreateCode;
import ldh.maker.util.FileUtil;
import ldh.maker.vo.TreeNode;

import java.io.File;

/**
 * Created by ldh on 2017/4/3.
 */
public class XmlPane extends CodePane {

    public XmlPane(TreeItem<TreeNode> treeItem, String javaName, String pack, CreateCode createCode) {
        super(treeItem, javaName, pack, createCode);
    }

    protected String getDir(CreateCode createCode) {
        return createCode.getProjectName() + "/src/main/resources";
    }
}
