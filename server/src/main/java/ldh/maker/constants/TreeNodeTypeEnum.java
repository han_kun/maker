package ldh.maker.constants;

/**
 * Created by ldh on 2017/2/19.
 */
public enum TreeNodeTypeEnum {
    ROOT,
    PROJECT,
    CODE,
    DATABASE,
    DB_CONNECTION,
    DB,
    DB_TABLE,
    DB_COLUMN,
    ITEM,
    JAVA_FILE,
    ;
}
