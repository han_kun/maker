package ldh.maker.component;

import javafx.scene.control.TreeItem;
import ldh.maker.vo.TreeNode;

/**
 * Created by ldh123 on 2018/5/6.
 */
public class SpringJavafxTableUi extends PojoTableUi {

    public SpringJavafxTableUi(TreeItem<TreeNode> treeItem, String dbName) {
        super(treeItem, dbName);
    }

}
