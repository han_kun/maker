<?xml version="1.0" encoding="UTF-8"?>
<ehcache>
    <diskStore path="D:/data/cache/resource"/>
    <defaultCache
            maxEntriesLocalHeap="10000"
            eternal="false"
            timeToIdleSeconds="120"
            timeToLiveSeconds="120"
            maxEntriesLocalDisk="10000000"
            diskExpiryThreadIntervalSeconds="120"
            memoryStoreEvictionPolicy="LRU">
        <persistence strategy="localTempSwap"/>
    </defaultCache>

    <cacheManagerPeerProviderFactory
            class="net.sf.ehcache.distribution.RMICacheManagerPeerProviderFactory"
            properties="peerDiscovery=automatic, multicastGroupAddress=224.0.10.1,
            multicastGroupPort=4446, timeToLive=32" />

    <cacheManagerPeerListenerFactory
            class="net.sf.ehcache.distribution.RMICacheManagerPeerListenerFactory"
            properties="port=40001,socketTimeoutMillis=2000" />

    <cache name="resourceAuthority_sso-demo" maxElementsInMemory="10000" eternal="false"
           timeToIdleSeconds="0" timeToLiveSeconds="3600" overflowToDisk="false"
           diskSpoolBufferSizeMB="60" maxElementsOnDisk="1000000"
           diskPersistent="false" diskExpiryThreadIntervalSeconds="120"
           memoryStoreEvictionPolicy="LRU">
        <cacheEventListenerFactory
                class="net.sf.ehcache.distribution.RMICacheReplicatorFactory"
                properties="replicateAsynchronously=true, replicatePuts=true, replicateUpdates=true,
                            replicateUpdatesViaCopy=true, replicateRemovals=true,asynchronousReplicationIntervalMillis=1000" />
        <bootstrapCacheLoaderFactory class="net.sf.ehcache.distribution.RMIBootstrapCacheLoaderFactory"/>
    </cache>

    <cache name="defaultCache" maxElementsInMemory="10000" eternal="false"
           timeToIdleSeconds="0" timeToLiveSeconds="500" overflowToDisk="true"
           diskSpoolBufferSizeMB="60" maxElementsOnDisk="1000000"
           diskPersistent="true" diskExpiryThreadIntervalSeconds="120"
           memoryStoreEvictionPolicy="LRU">
        <cacheEventListenerFactory
                class="net.sf.ehcache.distribution.RMICacheReplicatorFactory"
                properties="replicateAsynchronously=true, replicatePuts=true, replicateUpdates=true,
                            replicateUpdatesViaCopy=true, replicateRemovals=true,asynchronousReplicationIntervalMillis=1000" />
        <bootstrapCacheLoaderFactory class="net.sf.ehcache.distribution.RMIBootstrapCacheLoaderFactory"/>
    </cache>
</ehcache>