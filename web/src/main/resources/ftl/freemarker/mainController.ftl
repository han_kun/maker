package ${controllerPackage};

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
<#if shiro>
import org.apache.shiro.subject.Subject;
import org.apache.shiro.SecurityUtils;
</#if>

/**
 * @author: ${Author}
 * @date: ${DATE}
 */
@Controller
public class MainController {

    <#if shiro>
	@RequestMapping(method = RequestMethod.GET, value = "/")
    public String login() throws Exception {
        Subject subject = SecurityUtils.getSubject();
        if (subject.isAuthenticated()) {
            return "main";
        }
        return "login";
    }
    </#if>

    @RequestMapping(method = RequestMethod.GET, value = "/main")
    public String main() throws Exception {
        return "main";
    }
}