package ldh.maker;

import ldh.maker.component.EasyuiContentUiFactory;
import ldh.maker.util.UiUtil;

public class EasyuiWebMain extends ServerMain {

    @Override
    protected void preHandle() {
        UiUtil.setContentUiFactory(new EasyuiContentUiFactory());
        UiUtil.setType("easyui");
    }

    @Override
    protected String getTitle() {
        return "智能代码生成器之生成spring boot + easyui代码";
    }

    public static void main(String[] args) throws Exception {
        throw new RuntimeException("请运行MainLauncher");
//        startDb(null);
//        launch(args);
    }
}

