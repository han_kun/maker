package ldh.maker.component.oauth;

import javafx.scene.control.TreeItem;
import ldh.maker.component.CodeUi;
import ldh.maker.component.ColumnUi;
import ldh.maker.vo.TreeNode;

/**
 * Created by ldh on 2017/4/6.
 */
public class OauthResourceColumnUi extends ColumnUi {

    public OauthResourceColumnUi(TreeItem<TreeNode> treeItem, String dbName, String tableName, CodeUi codeUi) {
        super(treeItem, dbName, tableName, codeUi);
    }
}
