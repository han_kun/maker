package ldh.maker.component.oauth;

import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import ldh.database.Table;
import ldh.maker.code.CreateCode;
import ldh.maker.code.FreemarkerOauthCreateCode;
import ldh.maker.component.*;
import ldh.maker.vo.SettingData;
import ldh.maker.vo.TreeNode;

public class FreemarkerOauthContentUi extends TableUi {

    public FreemarkerOauthContentUi() {
    }

    @Override
    protected Tab createContentTab(String selectTable) {
        Tab tab = new Tab();
        tab.setText(selectTable);
        String db = treeItem.getValue().getData().toString();
        CodeUi codeUi = new FreemarkerOauthCodeUi(treeItem, db, selectTable);
        tab.setContent(codeUi);
        return tab;
    }

    @Override
    protected CreateCode buildCreateCode(SettingData data, TreeItem<TreeNode> treeItem, String dbName, Table table) {
        return new FreemarkerOauthCreateCode(data, treeItem, dbName, table);
    }

    @Override
    protected SettingPane buildSettingPane(TreeItem<TreeNode> treeItem, String dbName) {
        return new FreemarkerOauthSettingPane(treeItem, dbName);
    }
}
