import 'package:flutter/material.dart';
import 'dart:async';

class FuturePane extends StatelessWidget {
  FuturePane({Key key, this.loading, this.error, this.child, this.showType}) : super(key: key) {
    if (loading == null) {
      loading = const CircularProgressIndicator();
    }
    if (error == null) {
      error = _buildError();
    }
  }

  Widget loading;
  Widget child;
  Widget error;
  ShowType showType;

  @override
  Widget build(BuildContext context) {
    if (showType == ShowType.loading) {
      return new Center(
        child: loading,
      );
    } else if (showType == ShowType.error) {
      return new Center(
        child: error,
      );
    }
    return child;
  }

  Widget _buildError() {
    return new Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        new Text("加载失败!!!", style: const TextStyle(color: Colors.blueAccent, fontSize: 26.0),),
        new Icon(Icons.error, color: Colors.blueAccent, size: 56.0,),
      ],
    );
  }

}

enum ShowType {
  loading,
  error,
  data,
}