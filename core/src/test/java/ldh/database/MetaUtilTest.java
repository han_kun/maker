package ldh.database;

import javafx.application.Platform;
import ldh.database.util.MetaUtil;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class MetaUtilTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(MetaUtilTest.class);

    @Test
    public void getTable() throws Exception {
        String jdbcurl = "jdbc:mysql://localhost:3306/auth?useUnicode=true&characterEncoding=utf-8&serverTimezone=UTC";
        Class.forName("com.mysql.cj.jdbc.Driver");
        //连接数据库
        Connection conn = DriverManager.getConnection(jdbcurl, "root", "123456");

        Table table = MetaUtil.getTable(conn, "%", "auth", "authority");
        LOGGER.info("table:" + table.getForeignKeys());
        conn.close();
    }

    @Test
    public void all() throws Exception {
        String jdbcurl = "jdbc:mysql://localhost:3306/auth?useUnicode=true&characterEncoding=utf-8&serverTimezone=UTC";
        Class.forName("com.mysql.cj.jdbc.Driver");
        //连接数据库
        Connection conn = DriverManager.getConnection(jdbcurl, "root", "123456");
        DbInfo dbInfo = new DbInfo(conn, "auth");
        LOGGER.info("table:" + dbInfo.getTable("user"));
        for(Column column : dbInfo.getTable("user").getColumnList()) {
            LOGGER.info("column:" + column.getProperty());
        }
        conn.close();
    }
}
